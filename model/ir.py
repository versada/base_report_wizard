# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.
from openerp.osv import orm, fields
from openerp.tools.translate import _
import logging

logger = logging.getLogger(__name__)


class IrActionsReportXml(orm.Model):
    _inherit = 'ir.actions.report.xml'

    def _set_open_wizard(self, cr, uid, res_id, field_name, field_value, arg,
                         context=None):
        """
        Method to set value for functional field 'open_wizard'

        Changes ir_values and act_window values in order to initiate wizard
        window instead of calling report

        :param field_value: True or False
        :return: None
        """

        if field_value not in (True, False):
            return None

        ir_values_obj = self.pool.get('ir.values')
        act_win_obj = self.pool.get('ir.actions.act_window')
        trans_obj = self.pool.get('ir.translation')
        res_id = isinstance(res_id, list) and res_id[0] or res_id
        res_obj = self.browse(cr, uid, res_id, context=context)

        if field_value:
            name_suffix = _("multi copy")
            name = "%s (%s)" % (res_obj.name, name_suffix)
            action_data = {'name': name,
                           'view_mode': 'form',
                           'view_type': 'form',
                           'target': 'new',
                           'res_model': 'ir.actions.report.xml.wizard',
                           'context': {'ir_actions_report_xml_id': res_id}
                           }
            act_id = self.pool.get('ir.actions.act_window').create(
                cr, uid, action_data, context)

            ir_values_obj.set_action(
                cr, uid, name, 'client_print_multi', res_obj.model,
                'ir.actions.act_window,%s' % act_id)

            translations = trans_obj.search(
                cr, uid, [('res_id', '=', res_id), ('src', '=', res_obj.name),
                          ('name', '=', 'ir.actions.report.xml,name')],
                context=context
            )

            for trans in trans_obj.browse(cr, uid, translations, context):
                trans_obj.copy(
                    cr, uid, trans.id,
                    default={'name': 'ir.actions.act_window,name',
                             'res_id': act_id,
                             'value': "%s (%s)" % (trans.value, name_suffix)},
                    context=context
                )
        else:
            win_ids = act_win_obj.search(
                cr, uid, [('res_model', '=', 'ir.actions.report.xml.wizard')],
                context=context)

            for each in act_win_obj.browse(cr, uid, win_ids, context=context):
                act_win_ctx = eval(each.context, {})
                report_action_id = act_win_ctx.get('ir_actions_report_xml_id')
                if report_action_id == res_id:
                    unlink_id = each.id
                    break
            else:
                logger.error("Report for record %s not found" % res_id)
                return None

            act_win_obj.unlink(cr, uid, unlink_id, context=context)

        return None

    def _find_window_action_ids(self, cr, uid, ids, context=None):
        """
        Finds window actions for selected ids
        :param ids: report action ids to find window actions for
        :return: dict in a form
        {<id1>: <act_window ID or None>, <id2>: <act_window ID or None>, ...}
        """
        to_ret = {}.fromkeys(ids, None)
        act_win_obj = self.pool.get('ir.actions.act_window')
        values_pool = self.pool.get('ir.values')
        event_ids = act_win_obj.search(
            cr, uid, [('res_model', '=', 'ir.actions.report.xml.wizard')],
            context=context)

        for act_obj in act_win_obj.browse(cr, uid, event_ids, context=context):
            act_win_context = eval(act_obj.context, {})
            report_action_id = act_win_context.get('ir_actions_report_xml_id')
            if report_action_id in ids:
                event_id = values_pool.search(
                    cr, uid, [('value', '=', 'ir.actions.act_window,%s' %
                               act_obj.id)],
                    context=context)
                to_ret[report_action_id] = event_id and act_obj.id or None

        return to_ret

    def _get_open_wizard(self, cr, uid, ids, field_name, arg, context=None):
        """
        Method to get value for functional field 'open_wizard'
        """
        to_ret = {}.fromkeys(ids, False)
        act_ids = self._find_window_action_ids(cr, uid, ids, context)
        for key, val in act_ids.items():
            to_ret[key] = bool(val)
        return to_ret

    _columns = {
        'open_wizard': fields.function(
            _get_open_wizard, fnct_inv=_set_open_wizard,
            type='boolean', string="Report Wizard",
            help="Opens additional settings wizard before printing.")
    }

    def unlink(self, cr, uid, ids, context=None):
        act_ids = self._find_window_action_ids(cr, uid, ids, context=context)
        act_ids = filter(lambda item: bool(item[1]), act_ids.items())
        if act_ids:
            unlink_ids = dict(act_ids).values()
            self.pool.get('ir.actions.act_window').unlink(
                cr, uid, unlink_ids, context=context
            )

        return super(IrActionsReportXml, self).unlink(cr, uid, ids, context)
