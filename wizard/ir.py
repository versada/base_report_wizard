# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.
from openerp.osv import orm, fields
from openerp.tools.translate import _
import logging

logger = logging.getLogger(__name__)


class IrActionsReportXmlWizard(orm.TransientModel):
    _name = 'ir.actions.report.xml.wizard'

    _columns = {
        'copies': fields.integer("Number of copies", required=True),
        'tandem_printing': fields.boolean(
            string="Group",
            help="Same reports are grouped together and printed "
                 "next to each other.")
    }

    _defaults = {
        'copies': 1,
    }

    def button_print(self, cr, uid, ids, context=None):
        context = context or {}
        report_id = context.get('ir_actions_report_xml_id')
        active_ids = context.get('active_ids')
        if not report_id or not active_ids:
            logger.error("Printing error. Report id '%s'; Active ids '%s'" %
                         (report_id, active_ids))
            raise orm.except_orm(
                _("Error"),
                _("Unable to print report. Please contact system administrator")
            )

        obj = self.browse(cr, uid, ids[0], context=context)
        report_xml = self.pool.get('ir.actions.report.xml').browse(
            cr, uid, report_id, context=context
        )
        print_ids = active_ids
        if obj.copies > 1:
            if obj.tandem_printing:
                print_ids = []
                [print_ids.extend([x] * obj.copies) for x in active_ids]
            else:
                print_ids *= obj.copies

        data = {'model': report_xml.model,
                'ids': print_ids,
                'id': context['active_id']}
        if report_xml.report_type == 'controller':
            data = None
        return self.pool.get('report').get_action(
            cr, uid, print_ids, report_xml.report_name, data=data,
            context=context)
