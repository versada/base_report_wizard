/*---------------------------------------------------------
 * Use 20 results in Autocompletion field instead of default 7
 *---------------------------------------------------------*/

openerp.base_print_wizard = function(instance) {

    var QWeb = instance.web.qweb;
    var _t = instance.web._t;

    instance.web.Sidebar.include({
        add_items: function(section_code, items) {
            if (section_code === 'print') {
                items.sort(function (a, b) {
                    if (a.label < b.label) return -1;
                    if (b.label < a.label) return 1;
                    return 0;
                });
            };
            this._super(section_code, items);
        },
    });
};
