# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

{
    'name': 'Base Report Wizard',
    'version': '0.2',
    'author': 'Versada UAB',
    'category': 'Other',
    'website': 'http://www.versada.lt',
    'description': """
Open report wizard instead of printing directly
===============================================

This module adds an option to select reports which should open wizard instead of
being printed directly.

To enable wizard go to Settings -> Technical -> Actions -> Reports, find your
desired report and check 'Report Wizard'. When enabled new sub-menu under Print
will be available.
""",
    'depends': [
        'base',
    ],
    'data': [
    ],
    'update_xml': [
        'wizard/ir.xml',
        'view/ir.xml',
    ],
    'js': ['static/src/js/*.js'],
    'installable': True,
    'application': False,
}